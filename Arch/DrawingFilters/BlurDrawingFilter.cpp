#include <iostream>

#include "BlurDrawingFilter.h"

namespace drawer::drawing_filters
{
    BlurDrawingFilter::BlurDrawingFilter(std::unique_ptr<IPlaneDrawer> && plane_drawer)
            : m_plane_drawer(std::move(plane_drawer))
    {};

    void BlurDrawingFilter::draw(drawer::tools::Plane &plane)
    {
        m_plane_drawer->draw(plane);
        std::cout << "RadianceDrawingFilter::draw" << std::endl;
    }
}
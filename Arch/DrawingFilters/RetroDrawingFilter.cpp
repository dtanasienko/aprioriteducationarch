#include "RetroDrawingFilter.h"

namespace drawer::drawing_filters
{
    RetroDrawingFilter::RetroDrawingFilter(std::unique_ptr<IPlaneDrawer>&& plane_drawer)
            : m_plane_drawer(std::move(plane_drawer))
    {};

    void RetroDrawingFilter::draw(drawer::tools::Plane &plane)
    {
        m_plane_drawer->draw(plane);
        std::cout << "RetroDrawingFilter::draw" << std::endl;
    }
}
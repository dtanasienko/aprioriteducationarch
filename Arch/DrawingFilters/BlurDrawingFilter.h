#pragma once

#include <memory>

#include "RasterPlane/IPlaneDrawer.h"

namespace drawer::drawing_filters
{

    class BlurDrawingFilter : public IPlaneDrawer
    {
    public:
        explicit BlurDrawingFilter(std::unique_ptr<IPlaneDrawer>&& plane_drawer);

        void draw(tools::Plane& plane) override;

    private:
        std::unique_ptr<IPlaneDrawer> m_plane_drawer;
    };
}


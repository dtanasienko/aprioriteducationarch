#include "BsWDrawingFilter.h"

namespace drawer::drawing_filters
{
    BsWDrawingFilter::BsWDrawingFilter(std::unique_ptr<IPlaneDrawer> && plane_drawer)
            : m_plane_drawer(std::move(plane_drawer))
    {};

    void BsWDrawingFilter::draw(tools::Plane& plane)
    {
        m_plane_drawer->draw(plane);
        std::cout << "BsWDrawingFilter::draw" << std::endl;
    }
}
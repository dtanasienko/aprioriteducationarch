#include "RadianceDrawingFilter.h"

namespace drawer::drawing_filters
{
    RadianceDrawingFilter::RadianceDrawingFilter(std::unique_ptr<IPlaneDrawer>&& plane_drawer)
            : m_plane_drawer(std::move(plane_drawer))
    {};

    void RadianceDrawingFilter::draw(tools::Plane &plane)
    {
        m_plane_drawer->draw(plane);
        std::cout << "RadianceDrawingFilter::draw" << std::endl;
    }
}
#pragma once

#include <iostream>
#include <memory>

#include "RasterPlane/IPlaneDrawer.h"
#include "RasterPlane/Plane.h"

namespace drawer::drawing_filters
{

    class RetroDrawingFilter : public IPlaneDrawer
    {
    public:
        explicit RetroDrawingFilter(std::unique_ptr<IPlaneDrawer>&& plane_drawer);

        void draw(tools::Plane& plane) override;

    private:
        std::unique_ptr<IPlaneDrawer> m_plane_drawer;
    };
}
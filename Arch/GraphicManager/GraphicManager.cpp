#include "GraphicManager.h"
#include "EventMechanizm/EventManager/EventManager.h"
#include "States/CreationState.h"
#include "States/RedactorState.h"
#include "Commands/commands.h"

namespace drawer
{
    GraphicManager::GraphicManager(std::shared_ptr<widgets::Workspace> workspace)
            : m_event_manager(std::make_unique<EventManager<EVENT_TYPE, StateType>>()),
              m_workspace(std::move(workspace)),
              m_state(std::make_unique<states::CreationState>(*this))
    {}

    void GraphicManager::change_state(StateType state_type)
    {
        switch (state_type)
        {
            case StateType::CREATION:
                std::cout << "GraphicManager::change_state: CREATION" << std::endl;
                m_state = std::make_unique<states::CreationState>(*this);
                break;
            case StateType::REDACTOR:
                std::cout << "GraphicManager::change_state: REDACTOR" << std::endl;
                m_state = std::make_unique<states::RedactorState>(*this);
                break;
        }
        m_event_manager->notify(EVENT_TYPE::STATE_CHANGED, state_type);
    }

    void GraphicManager::change_position(int dx, int dy)
    {
        m_workspace->perform_command(std::make_unique<commands::MoveCommand>(m_workspace, dx, dy));
    }

    void GraphicManager::change_size(double scale)
    {
        m_workspace->perform_command(std::make_unique<commands::ScalingCommand>(m_workspace, scale));
    }

    void GraphicManager::change_color(tools::Color color)
    {
        m_workspace->perform_command(std::make_unique<commands::ChangeColorCommand>(m_workspace, color));
    }

    void GraphicManager::group_shapes()
    {
        m_workspace->perform_command(std::make_unique<commands::GroupCommand>(m_workspace));
    }

    void GraphicManager::mark_shape(const events::MouseEvent &event)
    {
        m_workspace->perform_command(std::make_unique<commands::MarkCommand>(m_workspace));
    }

    void GraphicManager::create_shape(std::unique_ptr<shapes::AbstractShape> shape)
    {
        m_workspace->perform_command(std::make_unique<commands::AddShapeCommand>(m_workspace, std::move(shape)));
    }

    void GraphicManager::key_pressed(const events::KeyEvent& key_event)
    {
        m_state->key_pressed(key_event);
    }

    void GraphicManager::mouse_pressed(const events::MouseEvent& mouse_event)
    {
        m_state->mouse_pressed(mouse_event);
    }
    void GraphicManager::undo()
    {
        m_workspace->undo();
    }

    void GraphicManager::redo()
    {
        m_workspace->redo();
    }

    void GraphicManager::subscribe(EVENT_TYPE event_type, std::shared_ptr<ISubscriber<StateType>> subscriber)
    {
        m_event_manager->subscribe(event_type, subscriber);
    }

    void GraphicManager::unsubscribe(std::shared_ptr<ISubscriber<StateType>> subscriber)
    {
        m_event_manager->unsubscribe(subscriber);
    }
}
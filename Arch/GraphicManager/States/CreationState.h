#pragma once

#include <Shapes/Line.h>
#include <Shapes/Rectangle.h>
#include <Shapes/Round.h>
#include "IAppState.h"
#include "GraphicManager/GraphicManager.h"

namespace drawer::states
{
    class CreationState : public IAppState
    {
    public:
        explicit CreationState(GraphicManager& context);

        void key_pressed(const events::KeyEvent& key_event) override;

        void mouse_pressed(const events::MouseEvent& mouse_event) override;
    private:
        GraphicManager& m_context;
    };
}
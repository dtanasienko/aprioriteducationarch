#pragma once

#include "IAppState.h"
#include "GraphicManager/GraphicManager.h"

namespace drawer::states
{
    class RedactorState : public IAppState
    {
    public:
        explicit RedactorState(GraphicManager& context);

        void key_pressed(const events::KeyEvent& key_event) override;

        void mouse_pressed(const events::MouseEvent& mouse_event) override;
    private:
        GraphicManager& m_context;
    };
}
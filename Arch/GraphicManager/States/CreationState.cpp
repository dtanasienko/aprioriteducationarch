#include "CreationState.h"

namespace drawer::states
{
    CreationState::CreationState(GraphicManager& context) : m_context(context)
    {}

    void CreationState::key_pressed(const events::KeyEvent& key_event)
    {
        switch (key_event.key_code)
        {
            case events::KeyEvent::KEY_CODES::ONE:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::ONE" << std::endl;
                m_context.create_shape(std::make_unique<shapes::Line>());
                break;
            case events::KeyEvent::KEY_CODES::TWO:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::TWO" << std::endl;
                m_context.create_shape(std::make_unique<shapes::Rectangle>());
                break;
            case events::KeyEvent::KEY_CODES::THREE:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::THREE" << std::endl;
                m_context.create_shape(std::make_unique<shapes::Round>());
                break;
            case events::KeyEvent::KEY_CODES::SHIFT:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::SHIFT" << std::endl;
                m_context.change_state(GraphicManager::StateType::REDACTOR);
                break;
            case events::KeyEvent::KEY_CODES::U:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::U" << std::endl;
                m_context.undo();
                break;
            case events::KeyEvent::KEY_CODES::P:
                std::cout << "CreationState::key_pressed: KeyEvent::KEY_CODES::P" << std::endl;
                m_context.redo();
                break;
        }
    }

    void CreationState::mouse_pressed(const events::MouseEvent& mouse_event)
    {
        /*Nothing TO DO*/
    }
}
#pragma once

#include <unordered_map>
#include <functional>

#include "EventMechanizm/Events/KeyEvent.h"
#include "EventMechanizm/Events/MouseEvent.h"


namespace drawer::states
{
    class IAppState
    {
    public:
        virtual void key_pressed(const events::KeyEvent& key_event) = 0;

        virtual void mouse_pressed(const events::MouseEvent& mouse_event) = 0;

        virtual ~IAppState() = default;
    };
}
#include "RedactorState.h"

namespace drawer::states
{
        RedactorState::RedactorState(GraphicManager& context) : m_context(context)
        {}

        void RedactorState::key_pressed(const events::KeyEvent& key_event)
        {
            switch (key_event.key_code)
            {
                case events::KeyEvent::KEY_CODES::PLUS:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::PLUS" << std::endl;
                    m_context.change_size(1.2);
                    break;
                case events::KeyEvent::KEY_CODES::MINUS:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::MINUS" << std::endl;
                    m_context.change_size(1 / 1.2);
                    break;
                case events::KeyEvent::KEY_CODES::R:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::R" << std::endl;
                    m_context.change_color(tools::Color::RED);
                    break;
                case events::KeyEvent::KEY_CODES::A:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::A" << std::endl;
                    m_context.group_shapes();
                    break;
                case events::KeyEvent::KEY_CODES::U:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::U" << std::endl;
                    m_context.undo();
                    break;
                case events::KeyEvent::KEY_CODES::P:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::P" << std::endl;
                    m_context.redo();
                    break;
                case events::KeyEvent::KEY_CODES::DOWN_BUTTON:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::DOWN_BUTTON" << std::endl;
                    m_context.change_position(0, -10);
                    break;
                case events::KeyEvent::KEY_CODES::TOP_ARROW:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::TOP_ARROW" << std::endl;
                    m_context.change_position(0, 10);
                    break;
                case events::KeyEvent::KEY_CODES::LEFT_ARROW:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::LEFT_ARROW" << std::endl;
                    m_context.change_position(-10, 0);
                    break;
                case events::KeyEvent::KEY_CODES::RIGHT_ARROW:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::RIGHT_ARROW" << std::endl;
                    m_context.change_position(10, 0);
                    break;
                case events::KeyEvent::KEY_CODES::SHIFT:
                    std::cout << "RedactorState::key_pressed: KeyEvent::KEY_CODES::SHIFT" << std::endl;
                    m_context.change_state(GraphicManager::StateType::CREATION);
                    break;
            }
        }

        void RedactorState::mouse_pressed(const events::MouseEvent& mouse_event)
        {
            switch (mouse_event.button)
            {
                case events::MouseEvent::MouseButton::LEFT:
                    std::cout << "RedactorState::mouse_pressed: MouseEvent::MouseButton::LEFT" << std::endl;
                    m_context.mark_shape(mouse_event);
                    break;
                case events::MouseEvent::MouseButton::RIGHT:
                    /* I didn't come up */
                    break;
            }
        }
}
#pragma once

#include "EventMechanizm/Interfaces/ISubscriber.h"
#include "GraphicManager/GraphicManager.h"

namespace drawer
{
    class MouseSubcriber : public ISubscriber<GraphicManager::StateType>
    {
    public:
        void handleEvent(GraphicManager::StateType &context) override;
    };
}
#include "NotificationSubscriber.h"

namespace drawer
{
    void NotificationSubcriber::handleEvent(GraphicManager::StateType &context)
    {
        switch (context)
        {
            case GraphicManager::StateType::CREATION:
                std::cout << "Drawer switched to mode Creation" << std::endl;
                break;
            case GraphicManager::StateType::REDACTOR:
                std::cout << "Drawer switched to mode Redactor" << std::endl;
                break;
        }
    }
}
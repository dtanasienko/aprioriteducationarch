#include "ToolbarSubscriber.h"

namespace drawer
{
    void ToolbarSubcriber::handleEvent(GraphicManager::StateType &context)
    {
        switch (context)
        {
            case GraphicManager::StateType::CREATION:
                std::cout << "Changing toolbar to Creation view" << std::endl;
                break;
            case GraphicManager::StateType::REDACTOR:
                std::cout << "Changing toolbar to Redactor view" << std::endl;
                break;
        }
    }
}
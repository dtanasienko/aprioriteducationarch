#include "MouseSubscriber.h"

namespace drawer
{
    void MouseSubcriber::handleEvent(GraphicManager::StateType &context)
    {
        switch (context)
        {
            case GraphicManager::StateType::CREATION:
                std::cout << "Changing mouse cursor to Creation view" << std::endl;
                break;
            case GraphicManager::StateType::REDACTOR:
                std::cout << "Changing mouse cursor to Redactor view" << std::endl;
                break;
        }
    }
}

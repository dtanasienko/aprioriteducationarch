#pragma once

#include <memory>

#include "States/IAppState.h"
#include "Widgets/Workspace.h"
#include "EventMechanizm/Events/MouseEvent.h"
#include "EventMechanizm/Interfaces/IEventManger.h"

namespace drawer
{
    class GraphicManager
    {
    public:
        enum class EVENT_TYPE {STATE_CHANGED};

        enum class StateType {CREATION, REDACTOR};

        explicit GraphicManager(std::shared_ptr<widgets::Workspace> workspace);

        void change_position(int dx, int dy);

        void change_size(double scale);

        void change_color(tools::Color color);

        void group_shapes();

        void mark_shape(const events::MouseEvent &event);

        void create_shape(std::unique_ptr<shapes::AbstractShape> shape);

        void change_state(StateType state_type);

        void undo();

        void redo();

        void key_pressed(const events::KeyEvent& key_event);

        void mouse_pressed(const events::MouseEvent& mouse_event);

        void subscribe(EVENT_TYPE event_type, std::shared_ptr<ISubscriber<StateType>> subscriber);

        void unsubscribe(std::shared_ptr<ISubscriber<StateType>> subscriber);

    private:
        std::shared_ptr<widgets::Workspace> m_workspace;
        std::unique_ptr<states::IAppState> m_state;
        std::unique_ptr<IEventManager<EVENT_TYPE, StateType>> m_event_manager;
    };
}

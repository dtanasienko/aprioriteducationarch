#pragma once

#include <memory>
#include <tuple>
#include <deque>
#include <iostream>

#include "Colors.h"
#include "RasterPlane/Plane.h"
#include "Point.h"
#include "GeometricSize.h"


namespace drawer::shapes
{
    class AbstractShape
    {
    public:
        AbstractShape(tools::Point point, const tools::GeometricSize &size, tools::Color color);

        virtual ~AbstractShape() = default;

        virtual void draw(tools::Plane &plane) const = 0;

        virtual std::unique_ptr<AbstractShape> clone() const = 0;

        virtual void set_color(tools::Color color);

        virtual tools::Color get_color() const;

        virtual void set_marked(bool marked);

        virtual bool get_marked() const;

        virtual void set_coordinetes(const tools::Point &point);

        virtual tools::Point get_coordinetes() const;

        virtual void move(int dx, int dy);

        virtual void set_size(const tools::GeometricSize &size);

        virtual tools::GeometricSize get_size() const;

        virtual void scaling(double rate);

    protected:
        bool m_marked;
        tools::Point m_point;
        tools::Color m_color;
        tools::GeometricSize m_geometric_size;
    };
}

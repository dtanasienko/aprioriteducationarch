#pragma once

#include "AbstractShape.h"

namespace drawer::shapes
{
    class Round : public AbstractShape
    {
    public:
        explicit Round(const tools::Point& point = tools::Point{15, 15},
                       const tools::GeometricSize& size = tools::GeometricSize{50, 50},
                       tools::Color color = tools::Color::BLACK);

        void draw(tools::Plane &plane) const override;

        std::unique_ptr<AbstractShape> clone() const override;
    };
}
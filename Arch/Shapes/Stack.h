#pragma once

#include "AbstractShape.h"

namespace drawer::shapes
{
    class Stack : public AbstractShape
    {
    public:
        explicit Stack(const tools::Point &point = tools::Point{15, 15});

        void draw(tools::Plane &plane) const override;

        void add_children(std::shared_ptr<AbstractShape> figure);

        void remove_children(const std::shared_ptr<AbstractShape> &figure);

        std::unique_ptr<AbstractShape> clone() const override;

        void set_color(tools::Color color) override;

        tools::Color get_color() const override;

    private:
        std::deque<std::shared_ptr<AbstractShape>> m_childrens;
    };
}
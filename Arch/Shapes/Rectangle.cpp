#include <iostream>

#include "Rectangle.h"

namespace drawer::shapes
{
    Rectangle::Rectangle(const tools::Point &point, const tools::GeometricSize &size, tools::Color color)
            : AbstractShape(point, size, color)
    {}

    void Rectangle::draw(tools::Plane &plane) const
    {
        std::cout << "Rectangle:" << "draw" << std::endl;
    }

    std::unique_ptr<AbstractShape> Rectangle::clone() const
    {
        std::cout << "Rectangle::clone" << std::endl;
        return std::make_unique<Rectangle>(m_point, m_geometric_size, m_color);
    }
}
//
// Created by Dezdor on 08.11.2018.
//
#include <iostream>

#include "Line.h"

namespace drawer::shapes
{
    Line::Line(const tools::Point &point, const tools::GeometricSize &size, tools::Color color)
            : AbstractShape(point, size, color)
    {}

    void Line::draw(tools::Plane &plane) const
    {
        std::cout << "Line:" << "draw" << std::endl;
    }

    std::unique_ptr<AbstractShape> Line::clone() const
    {
        std::cout << "Line::clone" << std::endl;
        return std::make_unique<Line>(m_point, m_geometric_size, m_color);
    }
}
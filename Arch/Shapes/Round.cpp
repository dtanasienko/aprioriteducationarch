#include <iostream>

#include "Round.h"

namespace drawer::shapes
{
    Round::Round(const tools::Point &point, const tools::GeometricSize &size, tools::Color color)
            : AbstractShape(point, size, color)
    {}

    void Round::draw(tools::Plane &plane) const
    {
        std::cout << "Round:" << "draw" << std::endl;
    }

    std::unique_ptr<AbstractShape> Round::clone() const
    {
        std::cout << "Round::clone" << std::endl;
        return std::make_unique<Round>(m_point, m_geometric_size, m_color);
    }
}
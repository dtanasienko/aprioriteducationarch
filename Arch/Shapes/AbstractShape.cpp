#include "AbstractShape.h"

namespace drawer::shapes
{
    AbstractShape::AbstractShape(tools::Point point, const tools::GeometricSize &size, tools::Color color)
            : m_point(point), m_geometric_size(size), m_color(color), m_marked(false)
    {}

    void AbstractShape::set_color(tools::Color color)
    {
        std::cout << "AbstractShape::set_color: " << static_cast<int>(color) << std::endl;
        m_color = color;
    }

    tools::Color AbstractShape::get_color() const
    {
        std::cout << "AbstractShape::get_color: " << static_cast<int>(m_color) << std::endl;
        return m_color;
    }

    void AbstractShape::set_marked(bool marked)
    {
        std::cout << "AbstractShape::set_marked: " << marked << std::endl;
        m_marked = marked;
    }

    bool AbstractShape::get_marked() const
    {
        std::cout << "AbstractShape::get_marked: " << m_marked << std::endl;
        return m_marked;
    }

    void AbstractShape::set_coordinetes(const tools::Point &point)
    {
        std::cout << "AbstractShape::set_coordinetes: (" << point.x << ", " << point.y << ")" << std::endl;
        m_point = point;
    }

    tools::Point AbstractShape::get_coordinetes() const
    {
        std::cout << "AbstractShape::get_coordinetes: (" << m_point.x << ", " << m_point.y << ")" << std::endl;
        return m_point;
    }

    void AbstractShape::move(int dx, int dy)
    {
        std::cout << "AbstractShape::move: dx=" << dx << " dy=" << dy << std::endl;
        this->set_coordinetes(tools::Point{this->m_point.x + dx, this->m_point.y + dy});
    }

    void AbstractShape::set_size(const tools::GeometricSize &size)
    {
        std::cout << "AbstractShape::set_size: size=(" << size.height << ", " << size.width << ")" << std::endl;
        m_geometric_size = size;
    }

    tools::GeometricSize AbstractShape::get_size() const
    {
        std::cout << "AbstractShape::get_size: size=(" << m_geometric_size.height << ", " << m_geometric_size.width << ")" << std::endl;
        return m_geometric_size;
    }

    void AbstractShape::scaling(double rate)
    {
        std::cout << "AbstractShape::scaling: rate=" << rate << std::endl;
        m_geometric_size = tools::GeometricSize{
                int(m_geometric_size.width * rate),
                int(m_geometric_size.height * rate)
        };
    }
}

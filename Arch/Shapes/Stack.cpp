#include <iostream>
#include <algorithm>

#include "Stack.h"

namespace drawer::shapes
{
    Stack::Stack(const tools::Point &point)
            : AbstractShape(point, tools::GeometricSize{0, 0}, tools::Color::TRANSPARENTS)
    {}

    void Stack::draw(tools::Plane &plane) const
    {
        std::cout << "Stack::draw:" << std::endl;
        for (const auto &children : m_childrens)
        {
            std::cout << "\t";
            children->draw(plane);
        }
    }

    void Stack::add_children(std::shared_ptr<AbstractShape> figure)
    {
        std::cout << "Stack::add_children" << std::endl;
        m_childrens.push_back(figure);
    }

    void Stack::remove_children(const std::shared_ptr<AbstractShape> &figure)
    {
        std::cout << "Stack::remove_children" << std::endl;
        m_childrens.erase(std::remove(m_childrens.begin(), m_childrens.end(), figure), m_childrens.end());
    }

    std::unique_ptr<AbstractShape> Stack::clone() const
    {
        std::cout << "Stack::clone" << std::endl;

        auto stack = std::make_unique<Stack>(m_point);

        for (const auto &children : m_childrens)
        {
            stack->add_children(children->clone());
        }

        return std::move(stack);
    }

    void Stack::set_color(tools::Color color)
    {
        throw std::runtime_error("Cannot set color for Stack");
    }

    tools::Color Stack::get_color() const
    {
        throw std::runtime_error("Cannot get color of Stack");
    }
}
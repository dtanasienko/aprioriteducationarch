#include "utils.h"

namespace drawer::tools
{

    GlobaMouse::GlobaMouse() : m_point(Point{42, 42}) {}

    GlobaMouse& GlobaMouse::get_instance()
    {
        static GlobaMouse globa_mouse;
        return globa_mouse;
    }

    Point GlobaMouse::get_mouse_position()
    {
        return get_instance().m_point;
    }

    void GlobaMouse::set_mouse_position(const Point& point)
    {
        get_instance().m_point = point;
    }
}
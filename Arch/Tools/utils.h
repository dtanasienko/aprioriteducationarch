#pragma once

#include "Point.h"

namespace drawer::tools
{
    class GlobaMouse
    {
    public:
        static Point get_mouse_position();

        static void set_mouse_position(const Point& point);
    private:
        GlobaMouse();

        static GlobaMouse& get_instance();

        Point m_point;
    };
}
#pragma once

namespace drawer::tools
{
    struct GeometricSize
    {
        int width;
        int height;
    };
}
#pragma once

#include <cmath>

namespace drawer::tools
{
    struct Point
    {
        int x;
        int y;
    };

    inline bool operator< (const Point& lhs, const Point& rhs)
    {
        return std::hypot(lhs.x, lhs.y) < std::hypot(rhs.x, rhs.y);
    }
}
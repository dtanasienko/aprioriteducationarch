#pragma once

namespace drawer::tools
{
    enum class Color
    {
        TRANSPARENTS, RED, BLUE, GREEN, BLACK, WHITE
    };
}



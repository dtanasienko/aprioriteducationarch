#pragma once

#include <memory>

#include "IPlaneDrawer.h"
#include "Painter.h"
#include "Plane.h"

namespace drawer
{
    class PlaneDrawer : public IPlaneDrawer
    {
    public:
        PlaneDrawer();

        void draw(tools::Plane& plane) override;

    private:
        std::unique_ptr<painter::IPainter> m_painter;
    };
}
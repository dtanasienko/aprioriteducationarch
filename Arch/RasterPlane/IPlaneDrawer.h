#pragma once

#include "Plane.h"

namespace drawer
{
    class IPlaneDrawer
    {
    public:
        virtual void draw(tools::Plane& plane) = 0;

        virtual ~IPlaneDrawer() = default;
    };
}
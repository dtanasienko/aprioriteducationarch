#include <iostream>

#include "PlaneDrawer.h"

namespace drawer
{
    PlaneDrawer::PlaneDrawer()
            : m_painter(painter::create_painter())
    {}

    void PlaneDrawer::draw(tools::Plane& plane)
    {
        std::cout << "PlaneDrawer::draw" << std::endl;
    }
}
#include "Button.h"

namespace drawer::widgets
{
    Button::Button(std::string_view text, std::shared_ptr<Workspace> workspace, std::shared_ptr<commands::ICommand> command)
        : m_text(text), m_workspace(std::move(workspace)), m_command(std::move(command))
    {}

    void Button::draw()
    {
        std::cout << "Button::" << "draw" << std::endl;
    };

    void Button::set_text(std::string_view text)
    {
        std::cout << "Button::set_text" << std::endl;
        m_text = text;
    }

    void Button::push()
    {
        std::cout << "Pushing button, text: " << m_text << std::endl;
        m_workspace->perform_command(std::move(m_command->clone()));
    }
}
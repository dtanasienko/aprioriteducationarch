#pragma once

#include <string>

#include "Widgets/AbstractWidget.h"

namespace drawer::widgets
{
    class Label : public AbstractWidget
    {
    public:
        explicit Label(std::string_view text);

        void draw() override;

        void set_text(std::string_view text);

        std::string get_text();
    private:
        std::string m_text;
    };
}
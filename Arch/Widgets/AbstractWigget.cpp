#include "AbstractWidget.h"

namespace drawer::widgets
{
    AbstractWidget::AbstractWidget(const tools::Point& point, const tools::GeometricSize& size)
            : m_position(point), m_geometric_size(size)
    {}

    void AbstractWidget::set_position(const tools::Point &pos)
    {
        std::cout << "AbstractShape::AbstractWidget::set_position: (" << pos.x << ", " << pos.y << ")" << std::endl;
        m_position = pos;
    }

    tools::Point AbstractWidget::get_position()
    {
        std::cout << "AbstractWidget::get_position: (" << m_position.x << ", " << m_position.y << ")" << std::endl;
        return m_position;
    }

    void AbstractWidget::move(int dx, int dy)
    {
        std::cout << "AbstractWidget::move: dx=" << dx << " dy=" << dy << std::endl;
        this->set_position(tools::Point{this->m_position.x + dx, this->m_position.y + dy});
    }

    tools::GeometricSize AbstractWidget::get_size()
    {
        std::cout << "AbstractWidget::get_size: [" << m_position.x << ", " << m_position.y << "]" << std::endl;
        return m_geometric_size;
    }

    void AbstractWidget::set_size(const tools::GeometricSize &size)
    {
        std::cout << "AbstractWidget::set_size: [" << m_position.x << ", " << m_position.y << "]" << std::endl;
        m_geometric_size = size;
    }
}

#pragma once

#include <string>
#include <memory>
#include <oledb.h>

#include "Widgets/AbstractWidget.h"
#include "Workspace.h"
#include "Commands/ICommand.h"

namespace drawer::widgets
{
    class Button : public AbstractWidget
    {
    public:
        Button(std::string_view text, std::shared_ptr<Workspace> workspace, std::shared_ptr<commands::ICommand> command);

        void draw() override;

        void set_text(std::string_view text);

        void push();
    private:
        std::string m_text;
        std::shared_ptr <Workspace> m_workspace;
        std::shared_ptr<commands::ICommand> m_command;
    };
}
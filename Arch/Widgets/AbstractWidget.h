#pragma once

#include <iostream>

#include "Point.h"
#include "IPainters.h"

namespace drawer::widgets
{
    class AbstractWidget
    {
    public:
        explicit AbstractWidget(const tools::Point& point = tools::Point{15,15},
                                const tools::GeometricSize& size = tools::GeometricSize{100, 100});

        virtual void draw() = 0;

        virtual void set_position(const tools::Point &pos);

        virtual void move(int dx, int dy);

        virtual tools::Point get_position();

        virtual tools::GeometricSize get_size();

        virtual void set_size(const tools::GeometricSize &size);

        virtual ~AbstractWidget() = default;

    private:
        tools::Point m_position;
        tools::GeometricSize m_geometric_size;
    };
}

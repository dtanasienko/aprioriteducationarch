#include <iostream>

#include "Lable.h"

namespace drawer::widgets
{
    Label::Label(std::string_view text) : m_text(text)
    {}

    void Label::draw()
    {
        std::cout << "TextField::draw" << std::endl;
    }

    void Label::set_text(std::string_view text)
    {
        std::cout << "Label::set_text: " << text << std::endl;
        m_text = text;
    }

    std::string Label::get_text()
    {
        std::cout << "Label::get_text: " << m_text << std::endl;
        return m_text;
    }
}
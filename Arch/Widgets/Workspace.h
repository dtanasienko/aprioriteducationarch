#pragma once

#include <vector>
#include <memory>

#include "Widgets/AbstractWidget.h"
#include "Shapes/AbstractShape.h"
#include "DrawingFilters/drawingfilters.h"
#include "Commands/ICommand.h"

namespace drawer::widgets
{
    class Workspace : public AbstractWidget
    {
    public:
        explicit Workspace(std::unique_ptr<IPlaneDrawer> drawing_filter);

        void draw() override;

        void set_draving_filter(std::unique_ptr<IPlaneDrawer> drawing_filter);

        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> get_selected();

        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> get_shape_by_mouse_position(const tools::Point& point);

        void remove_item(const std::shared_ptr<drawer::shapes::AbstractShape>& shape);

        void add_item(const std::shared_ptr<drawer::shapes::AbstractShape>& shape);

        void perform_command(std::unique_ptr<commands::ICommand>&& command);

        void undo();

        void redo();
    private:
        tools::Plane m_plane;
        std::deque<std::shared_ptr<drawer::shapes::AbstractShape>> m_childrens;
        std::unique_ptr<IPlaneDrawer> m_palne_drawer;
        std::vector<std::unique_ptr<drawer::commands::ICommand>> m_command_history;
        std::unique_ptr<painter::IPainter> m_painter;
        int m_curr_command_index;
    };
}

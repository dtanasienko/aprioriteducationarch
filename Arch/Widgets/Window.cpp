#include <iostream>
#include <algorithm>

#include "Window.h"

namespace drawer::widgets
{
    Window::Window() : m_painter(painter::create_painter())
    {}

    void Window::draw()
    {
        std::cout << "Window::" << "draw" << std::endl;
        m_painter->draw_rectangle(get_position(), get_size(), tools::Color::GREEN);
        for (auto& children : m_childrens)
        {
            children->draw();
        }
        auto painter = painter::create_painter();
    }

    void Window::add_children(std::shared_ptr<AbstractWidget> widget)
    {
        std::cout << "Window::add_children" << std::endl;
        m_childrens.push_back(widget);
    }

    void Window::remove_children(const std::shared_ptr<AbstractWidget> &widget)
    {
        std::cout << "Window::remove_children" << std::endl;
        m_childrens.erase(std::remove(m_childrens.begin(), m_childrens.end(), widget), m_childrens.end());
    }

    void Window::set_position(const tools::Point &pos)
    {
        std::cout << "Window::set_position: (" << pos.x << ", " << pos.y << ")" <<  std::endl;
        int dx = get_position().x - pos.x;
        int dy = get_position().y - pos.y;

        AbstractWidget::set_position(pos);

        for (auto &widget : m_childrens)
        {
            widget->move(dx, dy);
        }

    }
}
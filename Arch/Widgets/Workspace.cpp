#include <algorithm>

#include "Workspace.h"
#include "Painter.h"
#include "RasterPlane/IPlaneDrawer.h"

namespace drawer::widgets
{
    Workspace::Workspace(std::unique_ptr<IPlaneDrawer> drawing_filter)
            : m_palne_drawer(std::move(drawing_filter)), m_curr_command_index(-1), m_painter(painter::create_painter())
    {}

    void Workspace::set_draving_filter(std::unique_ptr<IPlaneDrawer> drawing_filter)
    {
        std::cout << "Workspace::set_draving_filter" << std::endl;
        m_palne_drawer = std::move(drawing_filter);
    }

    void Workspace::draw()
    {
        m_plane.clean();

        std::cout << "Workspace::draw" << std::endl;
        m_painter->draw_rectangle(get_position(), get_size(), tools::Color::TRANSPARENTS);
        m_painter->draw_line(tools::Point{get_position().x, get_position().y + 10},
                             tools::Point{get_position().x + get_size().width, get_position().y + 10},
                             tools::Color::TRANSPARENTS);
        for (const auto& shape : m_childrens)
        {
            shape->draw(m_plane);
        }
        m_palne_drawer->draw(m_plane);
    }

    std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> Workspace::get_selected()
    {
        std::cout << "Workspace::get_selected" << std::endl;
        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> selected_children;
        std::copy_if(m_childrens.cbegin(), m_childrens.cend(), std::back_inserter(selected_children),
                     [] (const auto& shape) { return shape->get_marked();});
        return std::move(selected_children);
    }

    std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> Workspace::get_shape_by_mouse_position(const tools::Point& point)
    {
        std::cout << "Workspace::get_shape_by_mouse_position: mouse position = (" << point.x << ", " << point.y << ")" << std::endl;
        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> shapes;
        for (const auto& shape : m_childrens)
        {
            if (shape->get_coordinetes().x < point.x && shape->get_coordinetes().x + shape->get_size().width > point.x ||
                shape->get_coordinetes().y < point.y && shape->get_coordinetes().y + shape->get_size().height > point.y)
            {
                shapes.push_back(shape);
            }
        }
        return std::move(shapes);
    }

    void Workspace::remove_item(const std::shared_ptr<drawer::shapes::AbstractShape>& shape)
    {
        std::cout << "Workspace::remove_item" << std::endl;
        m_childrens.erase(std::remove(m_childrens.begin(), m_childrens.end(), shape), m_childrens.end());
    }

    void Workspace::add_item(const std::shared_ptr<drawer::shapes::AbstractShape>& shape)
    {
        std::cout << "Workspace::add_item" << std::endl;
        m_childrens.push_back(shape);
    }

    void Workspace::undo()
    {
        std::cout << "Workspace::undo" << std::endl;
        if (m_curr_command_index > -1)
        {
            m_command_history[m_curr_command_index]->undo();
            m_curr_command_index--;
        }
    }

    void Workspace::redo()
    {
        std::cout << "Workspace::redo" << std::endl;
        if (m_curr_command_index < m_command_history.size())
        {
            m_command_history[m_curr_command_index]->undo();
            m_curr_command_index++;
        }
    }

    void Workspace::perform_command(std::unique_ptr<commands::ICommand>&& command)
    {
        std::cout << "Workspace::perform_command" << std::endl;
        try
        {
            command->execute();
            if (!m_command_history.empty())
            {
                m_command_history.erase(m_command_history.cbegin() + m_curr_command_index + 1, m_command_history.cend());
            }
            m_command_history.push_back(std::move(command));
            m_curr_command_index++;
        }
        catch (const std::exception& exc)
        {
            m_command_history.clear();
            std::cout << "Exception happened while performing command: " << exc.what() << std::endl;
        }
    }
}
#include <iostream>

#include "TextField.h"

namespace drawer::widgets
{
    TextField::TextField(std::string_view placebo_text) : m_placebo_text(placebo_text)
    {}

    void TextField::draw()
    {
        std::cout << "TextField::draw" << std::endl;
    }

    void TextField::set_placebo_text(std::string_view text)
    {
        std::cout << "TextField::set_placebo_text: " << text << std::endl;
        m_placebo_text = text;
    }

    std::string TextField::get_text()
    {
        std::cout << "TextField::get_text: " << m_text << std::endl;
        return  m_text;
    }

}
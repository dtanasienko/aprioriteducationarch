#pragma once

#pragma once

#include <string>

#include "Widgets/AbstractWidget.h"

namespace drawer::widgets
{
    class TextField : public AbstractWidget
    {
    public:
        explicit TextField(std::string_view placebo_text);

        void draw() override;

        void set_placebo_text(std::string_view text);

        std::string get_text();
    private:
        std::string m_text;
        std::string m_placebo_text;
    };
}
#pragma once

#include <memory>
#include <deque>

#include "Widgets/AbstractWidget.h"
#include "Painter.h"

namespace drawer::widgets
{
    class Window : public AbstractWidget
    {
    public:
        Window();

        void draw() override;

        void set_position(const tools::Point &pos) override;

        void add_children(std::shared_ptr<AbstractWidget> widget);

        void remove_children(const std::shared_ptr<AbstractWidget> &widget);

    private:
        std::deque<std::shared_ptr<AbstractWidget>> m_childrens;
        std::unique_ptr<painter::IPainter> m_painter;
    };
}
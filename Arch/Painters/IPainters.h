#pragma once

#include "Point.h"
#include "Colors.h"
#include "GeometricSize.h"

namespace drawer::painter
{
    class IPainter
    {
    public:
        virtual void draw_line(const tools::Point &start_point, const tools::Point &end_point, tools::Color color) = 0;

        virtual void draw_dot(const tools::Point &point, tools::Color color) = 0;

        virtual void draw_rectangle(const tools::Point &point, const tools::GeometricSize &size, tools::Color color) = 0;

        virtual ~IPainter() = default;
    };
}
#include <iostream>

#include "UbuntuPainter.h"

namespace drawer::painter
{
    void UbuntuPainter::draw_line(const tools::Point &start_point, const tools::Point &end_point, tools::Color color)
    {
        std::cout << "UbuntuPainter::" << "draw_line" << std::endl;
    }

    void UbuntuPainter::draw_dot(const tools::Point &point, tools::Color color)
    {
        std::cout << "UbuntuPainter::" << "draw_dot" << std::endl;
    }

    void UbuntuPainter::draw_rectangle(const tools::Point &point, const tools::GeometricSize &size, tools::Color color)
    {
        std::cout << "UbuntuPainter::" << "draw_rectangle" << std::endl;
    }
}
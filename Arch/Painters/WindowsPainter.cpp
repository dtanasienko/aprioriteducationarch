#include <iostream>

#include "WindowsPainter.h"

namespace drawer::painter
{
    void WindowsPainter::draw_line(const tools::Point &start_point, const tools::Point &end_point, tools::Color color)
    {
        std::cout << "WindowsPainter::" << "draw_line" << std::endl;
    }

    void WindowsPainter::draw_dot(const tools::Point &point, tools::Color color)
    {
        std::cout << "WindowsPainter::" << "draw_dot" << std::endl;
    }

    void WindowsPainter::draw_rectangle(const tools::Point &point, const tools::GeometricSize &size, tools::Color color)
    {
        std::cout << "WindowsPainter::" << "draw_rectangle" << std::endl;
    }
}
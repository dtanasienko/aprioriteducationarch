#pragma once

#include "IPainters.h"

namespace drawer::painter
{
    class WindowsPainter : public IPainter
    {
    public:
        void draw_line(const tools::Point &start_point, const tools::Point &end_point, tools::Color color) override;

        void draw_dot(const tools::Point &point, tools::Color color) override;

        void draw_rectangle(const tools::Point &point, const tools::GeometricSize &size, tools::Color color) override;
    };
}


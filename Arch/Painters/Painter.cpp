#include <memory>

#include "Painter.h"

#ifdef WIN32
    #include "WindowsPainter.h"
#elif UNIX
    #include "UbuntuPainter.h"
#endif

namespace drawer::painter
{
    std::unique_ptr<IPainter> create_painter()
    {
#ifdef WIN32
        return std::make_unique<WindowsPainter>();
#elif UNIX
        return std::make_unique<UbuntuPainter>();
#endif
    }
}
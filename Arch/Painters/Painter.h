#pragma once

#include <memory>

#include "IPainters.h"

namespace drawer::painter
{
    std::unique_ptr<IPainter> create_painter();
}


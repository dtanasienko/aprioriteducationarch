#pragma once

#include <memory>
#include <vector>

#include "ICommand.h"
#include "Widgets/Workspace.h"
#include "Shapes/shapes.h"

namespace drawer::commands
{
    class RemoveCommand : public ICommand
    {
    public:
        explicit RemoveCommand(std::shared_ptr<widgets::Workspace> workspace);

        void execute() override;

        void undo() override;

        std::unique_ptr<ICommand> clone() override;

    private:
        void clear();

    private:
        std::shared_ptr<widgets::Workspace> m_workspace;
        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> m_removed_shapes;
    };
}
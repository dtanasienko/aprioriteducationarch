#include "MarkCommand.h"

namespace drawer::commands
{
    MarkCommand::MarkCommand(std::shared_ptr<widgets::Workspace> workspace) : m_workspace(std::move(workspace))
    {}

    void MarkCommand::execute()
    {
        std::cout << "MarkCommand::execute" << std::endl;
        m_current_shapes = m_workspace->get_shape_by_mouse_position(tools::GlobaMouse::get_mouse_position());

        if (m_current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : m_current_shapes)
        {
            shape->set_marked(true);
        }
    }

    void MarkCommand::undo()
    {
        std::cout << "MarkCommand::undo" << std::endl;
        for (const auto &shape : m_current_shapes)
        {
            shape->set_marked(false);
        }
        clear();
    }

    std::unique_ptr<ICommand> MarkCommand::clone()
    {
        std::cout << "MarkCommand::clone" << std::endl;
        return std::make_unique<MarkCommand>(m_workspace);
    }

    void MarkCommand::clear()
    {
        m_current_shapes.clear();
    }
}
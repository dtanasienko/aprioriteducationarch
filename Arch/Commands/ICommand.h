#pragma once

#include <memory>

namespace drawer::commands
{
    class ICommand
    {
    public:
        virtual void execute() = 0;
        virtual void undo() = 0;

        virtual std::unique_ptr<ICommand> clone() = 0;

        virtual ~ICommand() = default;
    };
}
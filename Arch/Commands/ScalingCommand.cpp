#include "ScalingCommand.h"

namespace drawer::commands
{
    ScalingCommand::ScalingCommand(std::shared_ptr<widgets::Workspace> workspace, double rate) : m_workspace(
            std::move(workspace)), m_rate(rate)
    {}

    void ScalingCommand::execute()
    {
        std::cout << "ScalingCommand::execute" << std::endl;
        m_current_shapes = m_workspace->get_selected();

        if (m_current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : m_current_shapes)
        {
            shape->scaling(m_rate);
        }
    }

    void ScalingCommand::undo()
    {
        std::cout << "ScalingCommand::undo" << std::endl;
        for (const auto &shape : m_current_shapes)
        {
            shape->scaling(1 / m_rate);
        }

        clear();
    }

    std::unique_ptr<ICommand> ScalingCommand::clone()
    {
        std::cout << "ScalingCommand::clone" << std::endl;
        return std::make_unique<ScalingCommand>(m_workspace, m_rate);
    }

    void ScalingCommand::clear()
    {
        m_current_shapes.clear();
    }
}
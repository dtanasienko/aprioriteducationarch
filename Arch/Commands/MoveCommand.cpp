#include "MoveCommand.h"

namespace drawer::commands
{
    MoveCommand::MoveCommand(std::shared_ptr<widgets::Workspace> workspace, int dx, int dy) : m_workspace(
            std::move(workspace)), m_dx(dx), m_dy(dy)
    {}

    void MoveCommand::execute()
    {
        std::cout << "MoveCommand::execute" << std::endl;
        m_current_shapes = m_workspace->get_selected();

        if (m_current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : m_current_shapes)
        {
            shape->move(m_dx, m_dy);
        }
    }

    void MoveCommand::undo()
    {
        std::cout << "MoveCommand::undo" << std::endl;
        for (const auto &shape : m_current_shapes)
        {
            shape->move(-m_dx, -m_dy);
        }
        clear();
    }

    std::unique_ptr<ICommand> MoveCommand::clone()
    {
        std::cout << "MoveCommand::clone" << std::endl;
        return std::make_unique<MoveCommand>(m_workspace, m_dx, m_dy);
    }

    void MoveCommand::clear()
    {
        m_current_shapes.clear();
    }
}
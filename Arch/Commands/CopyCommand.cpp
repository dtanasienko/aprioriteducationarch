#include "CopyCommand.h"

namespace drawer::commands
{
    CopyCommand::CopyCommand(std::shared_ptr<widgets::Workspace> workspace) : m_workspace(std::move(workspace))
    {}

    void CopyCommand::execute()
    {
        std::cout << "CopyCommand::execute" << std::endl;
        auto current_shapes = m_workspace->get_selected();

        if (current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : current_shapes)
        {
            auto copy_shape = shape->clone();
            copy_shape->move(15, 15);

            m_copied_shapes.push_back(shape);
            m_workspace->add_item(shape);
        }
    }

    void CopyCommand::undo()
    {
        std::cout << "CopyCommand::undo" << std::endl;
        for (const auto &shape : m_copied_shapes)
        {
            m_workspace->remove_item(shape);
        }
        clear();
    }

    std::unique_ptr<ICommand> CopyCommand::clone()
    {
        std::cout << "CopyCommand::clone" << std::endl;
        return std::make_unique<CopyCommand>(m_workspace);
    }

    void CopyCommand::clear()
    {
        m_copied_shapes.clear();
    }
}


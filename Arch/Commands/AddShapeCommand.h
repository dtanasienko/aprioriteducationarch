#pragma once

#include <memory>
#include <vector>

#include "ICommand.h"
#include "Widgets/Workspace.h"
#include "Shapes/shapes.h"

namespace drawer::commands
{
    class AddShapeCommand : public ICommand
    {
    public:
        explicit AddShapeCommand(std::shared_ptr<widgets::Workspace> workspace,
                                 std::shared_ptr<shapes::AbstractShape> shape);

        void execute() override;

        void undo() override;

        std::unique_ptr<ICommand> clone() override;
    private:
        std::shared_ptr<widgets::Workspace> m_workspace;
        std::shared_ptr<shapes::AbstractShape> m_added_shape;
    };
}
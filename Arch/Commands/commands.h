#pragma once

#include "AddShapeCommand.h"
#include "ChangeColorCommand.h"
#include "CopyCommand.h"
#include "GroupCommand.h"
#include "MarkCommand.h"
#include "MoveCommand.h"
#include "RemoveCommand.h"
#include "ScalingCommand.h"
#include "AddShapeCommand.h"

namespace drawer::commands
{
    AddShapeCommand::AddShapeCommand(std::shared_ptr<widgets::Workspace> workspace, std::shared_ptr<shapes::AbstractShape> shape)
                : m_workspace(std::move(workspace)), m_added_shape(std::move(shape))
        {}
    void AddShapeCommand::execute()
    {
        std::cout << "AddShapeCommand::execute" << std::endl;
        m_workspace->add_item(m_added_shape);
    }

    void AddShapeCommand::undo()
    {
        std::cout << "AddShapeCommand::undo" << std::endl;
        m_workspace->remove_item(m_added_shape);
    }

    std::unique_ptr<ICommand> AddShapeCommand::clone()
    {
        std::cout << "AddShapeCommand::clone" << std::endl;
        return std::make_unique<AddShapeCommand>(m_workspace, m_added_shape);
    }
}
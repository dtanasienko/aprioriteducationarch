#include "RemoveCommand.h"

namespace drawer::commands
{
    RemoveCommand::RemoveCommand(std::shared_ptr<widgets::Workspace> workspace) : m_workspace(std::move(workspace))
    {}

    void RemoveCommand::execute()
    {
        std::cout << "RemoveCommand::execute" << std::endl;
        auto current_shapes = m_workspace->get_selected();

        if (current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : current_shapes)
        {
            m_removed_shapes.push_back(shape);
            m_workspace->remove_item(shape);
        }
    }

    void RemoveCommand::undo()
    {
        std::cout << "RemoveCommand::undo" << std::endl;
        for (const auto &shape : m_removed_shapes)
        {
            m_workspace->add_item(shape);
        }
        clear();
    }

    std::unique_ptr<ICommand> RemoveCommand::clone()
    {
        std::cout << "RemoveCommand::clone" << std::endl;
        return std::make_unique<RemoveCommand>(m_workspace);
    }

    void RemoveCommand::clear()
    {
        m_removed_shapes.clear();
    }
}

#include "GroupCommand.h"

namespace drawer::commands
{
    GroupCommand::GroupCommand(std::shared_ptr<widgets::Workspace> workspace) : m_workspace(std::move(workspace))
    {}

    void GroupCommand::execute()
    {
        std::cout << "GroupCommand::execute" << std::endl;
        m_current_shapes = m_workspace->get_selected();
        if (m_current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto &shape : m_current_shapes)
        {
            m_workspace->remove_item(shape);
        }

        auto min_by_coor = std::min_element(m_current_shapes.cbegin(), m_current_shapes.cend(),
                                            [](const auto &lhs, const auto &rhs)
                                                {
                                                    return lhs->get_coordinetes() < rhs->get_coordinetes();
                                                });
        m_created_stack = std::make_shared<shapes::Stack>();

        for (const auto &shape : m_current_shapes)
        {
            m_created_stack->add_children(shape);
        }

        m_workspace->add_item(m_created_stack);
    }

    void GroupCommand::undo()
    {
        std::cout << "GroupCommand::undo" << std::endl;
        m_workspace->remove_item(m_created_stack);

        for (const auto &shape : m_current_shapes)
        {
            m_workspace->add_item(shape);
        }
        clear();
    }

    std::unique_ptr<ICommand> GroupCommand::clone()
    {
        std::cout << "GroupCommand::clone" << std::endl;
        return std::make_unique<GroupCommand>(m_workspace);
    }

    void GroupCommand::clear()
    {
        m_created_stack.reset();
        m_current_shapes.clear();
    }
}
#pragma once

#include <memory>
#include <vector>

#include "ICommand.h"
#include "Widgets/Workspace.h"
#include "Shapes/shapes.h"

namespace drawer::commands
{
    class ChangeColorCommand : public ICommand
    {
    public:
        ChangeColorCommand(std::shared_ptr<widgets::Workspace> workspace, tools::Color color);

        void execute() override;

        void undo() override;

        std::unique_ptr<ICommand> clone() override;

    private:
        void clear();

    private:
        std::shared_ptr<widgets::Workspace> m_workspace;
        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> m_current_shapes;
        std::vector<tools::Color> m_saved_colors;
        tools::Color m_settable_color;
    };
}
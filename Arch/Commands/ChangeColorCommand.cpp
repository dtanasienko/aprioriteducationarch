#include "ChangeColorCommand.h"

namespace drawer::commands
{
    ChangeColorCommand::ChangeColorCommand(std::shared_ptr<widgets::Workspace> workspace, tools::Color color)
                : m_workspace(std::move(workspace)), m_settable_color(color)
        {}

    void ChangeColorCommand::execute()
    {
        std::cout << "ChangeColorCommand::execute" << std::endl;
        m_current_shapes = m_workspace->get_selected();

        if (m_current_shapes.empty())
        {
            throw std::runtime_error("Hasn't selected shapes");
        }

        for (const auto& shape : m_current_shapes)
        {
            m_saved_colors.push_back(shape->get_color());
            shape->set_color(m_settable_color);
        }
    }

    void ChangeColorCommand::undo()
    {
        std::cout << "ChangeColorCommand::undo" << std::endl;
        for (int i = 0; i < m_saved_colors.size(); ++i)
        {
            m_current_shapes[i]->set_color(m_saved_colors[i]);
        }
        clear();
    }

    std::unique_ptr<ICommand> ChangeColorCommand::clone()
    {
        std::cout << "ChangeColorCommand::clone" << std::endl;
        return std::make_unique<ChangeColorCommand>(m_workspace, m_settable_color);
    }

    void ChangeColorCommand::clear()
    {
        m_current_shapes.clear();
    }
}
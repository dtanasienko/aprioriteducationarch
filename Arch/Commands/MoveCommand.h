#pragma once

#include <memory>
#include <vector>

#include "ICommand.h"
#include "Widgets/Workspace.h"
#include "Shapes/shapes.h"

namespace drawer::commands
{
    class MoveCommand : public ICommand
    {
    public:
        MoveCommand(std::shared_ptr<widgets::Workspace> workspace, int dx, int dy);

        void execute() override;

        void undo() override;

        std::unique_ptr<ICommand> clone() override;

    private:
        void clear();

    private:
        std::shared_ptr<widgets::Workspace> m_workspace;
        std::vector<std::shared_ptr<drawer::shapes::AbstractShape>> m_current_shapes;
        int m_dx;
        int m_dy;
    };
}
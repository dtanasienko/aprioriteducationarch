#include <iostream>

#include "Shapes/shapes.h"
#include "Widgets/widgets.h"
#include "Commands/commands.h"
#include "GraphicManager/GraphicManager.h"
#include "DrawingFilters/drawingfilters.h"
#include "RasterPlane/IPlaneDrawer.h"
#include "RasterPlane/PlaneDrawer.h"
#include "GraphicManager/Subscribers/subscribers.h"

using namespace drawer;

std::unique_ptr<drawer::IPlaneDrawer> get_plane_drawer()
{
    return std::make_unique<drawing_filters::BsWDrawingFilter>(
            std::make_unique<drawing_filters::RadianceDrawingFilter>(
                    std::make_unique<drawing_filters::BlurDrawingFilter>(
                            std::make_unique<drawing_filters::RetroDrawingFilter>(
                                    std::make_unique<PlaneDrawer>()
                            )
                    )
            )
    );
}

void event_loop(GraphicManager& graphic_manager, std::shared_ptr<widgets::Workspace> workspace)
{
    widgets::Button button1("Button One",
                            workspace,
                            std::make_unique<commands::AddShapeCommand>(
                                    workspace,
                                    std::make_unique<shapes::Rectangle>(
                                            tools::Point{15, 15},
                                            tools::GeometricSize{100, 100},
                                            tools::Color::BLACK)
                            )
    );

    widgets::Button button12("Button OneTwo",
                            workspace,
                            std::make_unique<commands::AddShapeCommand>(
                                    workspace,
                                    std::make_unique<shapes::Round>(
                                            tools::Point{200, 200},
                                            tools::GeometricSize{10, 10},
                                            tools::Color::BLACK)
                            )
    );

    widgets::Button button2("Button Two", workspace, std::make_unique<commands::MarkCommand>(workspace));


    button1.push();
    button2.push();
    button12.push();

    tools::GlobaMouse::set_mouse_position(tools::Point{205,205});

    button2.push();

    graphic_manager.key_pressed(events::KeyEvent{events::KeyEvent::KEY_CODES::SHIFT});

    graphic_manager.key_pressed(events::KeyEvent{events::KeyEvent::KEY_CODES::RIGHT_ARROW});
    graphic_manager.key_pressed(events::KeyEvent{events::KeyEvent::KEY_CODES::P});
}

int main() {

    std::shared_ptr<widgets::Workspace> workspace = std::make_unique<widgets::Workspace>(get_plane_drawer());

    widgets::Window window;
    std::shared_ptr<widgets::Label> label = std::make_shared<widgets::Label>("Text on label");
    widgets::TextField text_filed("enter some text");

    std::shared_ptr<ToolbarSubcriber> toolbar_subcriber = std::make_shared<ToolbarSubcriber>();
    std::shared_ptr<NotificationSubcriber> notification_subcriber = std::make_shared<NotificationSubcriber>();
    std::shared_ptr<MouseSubcriber> mouse_subcriber = std::make_shared<MouseSubcriber>();


    GraphicManager graphic_manager(workspace);

    graphic_manager.subscribe(GraphicManager::EVENT_TYPE::STATE_CHANGED, toolbar_subcriber);
    graphic_manager.subscribe(GraphicManager::EVENT_TYPE::STATE_CHANGED, notification_subcriber);
    graphic_manager.subscribe(GraphicManager::EVENT_TYPE::STATE_CHANGED, mouse_subcriber);

    event_loop(graphic_manager, workspace);


    window.add_children(workspace);
    window.add_children(label);
    window.draw();
    return 0;
}
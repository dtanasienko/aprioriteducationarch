#pragma once

namespace drawer::events
{
    struct KeyEvent
    {
        enum class KEY_CODES
        {
            R, A,
            U, P,
            ONE, TWO, THREE,
            MINUS, PLUS,
            LEFT_ARROW, TOP_ARROW, RIGHT_ARROW, DOWN_BUTTON,
            SHIFT
        };
        KEY_CODES key_code;
    };
}
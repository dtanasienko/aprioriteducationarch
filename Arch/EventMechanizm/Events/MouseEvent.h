#pragma once

#include "Point.h"

namespace drawer::events
{
    struct MouseEvent
    {
        enum class MouseButton {LEFT, RIGHT};

        MouseButton button;
        tools::Point point;
    };
}
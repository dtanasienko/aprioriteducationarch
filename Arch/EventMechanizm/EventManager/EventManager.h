#pragma once

#include <unordered_map>
#include <deque>

#include <algorithm>

#include "EventMechanizm/Interfaces/IEventManger.h"

namespace drawer
{
    template <typename EventType, typename Observable>
    class EventManager : public IEventManager<EventType, Observable>
    {
    public:
        void subscribe(EventType event_type, std::shared_ptr<ISubscriber<Observable>> subscriber) override;

        void unsubscribe(std::shared_ptr<ISubscriber<Observable>> subscriber) override;

        void notify(EventType event, Observable& observable) override;

    private:
        std::unordered_map<EventType, std::deque<std::shared_ptr<ISubscriber<Observable>>>> m_subscribers;
    };
}

#include "EventManagerImpl.h"
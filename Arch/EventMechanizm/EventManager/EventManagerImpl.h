#pragma once

#include "EventManager.h"

namespace drawer
{
    template <typename EventType, typename Observable>
    void EventManager<EventType, Observable>::subscribe(EventType event_type, std::shared_ptr<ISubscriber<Observable>> subscriber)
    {
        std::cout << "EventManager::subscribe - EventType: " << static_cast<int>(event_type) << std::endl;
        auto it = std::find(m_subscribers[event_type].cbegin(), m_subscribers[event_type].cend(), subscriber);
        if (it != m_subscribers[event_type].end())
        {
            throw std::runtime_error("Already subscribed");
        }
        m_subscribers[event_type].push_back(subscriber);
    }

    template <typename EventType, typename Observable>
    void EventManager<EventType, Observable>::unsubscribe(std::shared_ptr<ISubscriber<Observable>> subscriber)
    {
        std::cout << "EventManager::unsubscribe " << std::endl;
        for (auto& [event, subscribers] : m_subscribers)
        {
            auto it = std::find(m_subscribers[event].cbegin(), m_subscribers[event].cend(), subscriber);
            if (it == m_subscribers[event].cend())
            {
                throw std::runtime_error("Subscriber haven't subscribed");
            }
            else
            {
                m_subscribers[event].erase(it);
                break;
            }
        }
    }

    template <typename EventType, typename Observable>
    void EventManager<EventType, Observable>::notify(EventType event, Observable& observable)
    {
        std::cout << "EventManager::notify" << std::endl;
        for (auto& subscribers : m_subscribers[event])
        {
            subscribers->handleEvent(observable);
        }
    }
}


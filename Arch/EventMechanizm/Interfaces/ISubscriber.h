#pragma once

namespace drawer
{
    template <typename Observable>
    class ISubscriber
    {
    public:
        virtual void handleEvent(Observable& observable) = 0;

        virtual ~ISubscriber() = default;
    };
}
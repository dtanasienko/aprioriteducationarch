#pragma once

#include <memory>

#include "EventMechanizm/Interfaces/ISubscriber.h"

namespace drawer
{
    template <typename EventType, typename Observable>
    class IEventManager
    {
    public:
        virtual void subscribe(EventType event_type, std::shared_ptr<ISubscriber<Observable>> subscriber) = 0;

        virtual void unsubscribe(std::shared_ptr<ISubscriber<Observable>> subscriber) = 0;

        virtual void notify(EventType event, Observable& observable) = 0;

        virtual ~IEventManager() = default;
    };
}
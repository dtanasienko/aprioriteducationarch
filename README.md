# AprioritEducationArch

![Arch](images/arc.png)

Используемые паттерны
---
**Мост** - используется для возможности кросплатфрмненной отрисовки елементов программы:

* Implementator - IPainter : для каждой платформы совой. Отвечает за рисование примитивов графики. (С помощью него в том числе рисуются окна);
* Abstraction - Widgets : элементы интерфейса пользователя.

**Composite** - используется для возможности эдинообразной работы с примитивами графики и их компоновки (также применяется и для виджетов):

* Component - AbstractShape, AbstractWidget
* Copmposite - Stack, Window
* Leaf - Ellipse, Rectangle, Line, Button, Lable, TextFiled

**Decorator** - используется для возможности комбинации нескольких фильтров.

**Command** - используется для возможности выполнения примых и обратных действий над примитивами.

**State** - используется для изменение реакции приложения на события клавиатуры и мыши.

**Observer** - используется для оповищения компонентов об изменении состояния редактора.

**Prototype** - используется для копирования примитивов графики, в том числе и структурных, для клонирования команд.
    
---
Диаграмма паттернов: https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=arc#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1EfM4SZ8pbmW1AWrRI9fFtFebj14rJ9oz%26export%3Ddownload